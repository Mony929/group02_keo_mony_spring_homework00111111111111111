package com.example.demo;

import java.time.LocalDateTime;

public class CustomerResponse<T> {
    private LocalDateTime dateTime;
    private Integer status;
    private String message;
    T customer;

    public CustomerResponse(LocalDateTime dateTime, Integer status, String message, T customer) {
        this.dateTime = dateTime;
        this.status = status;
        this.message = message;
        this.customer = customer;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getCustomer() {
        return customer;
    }

    public void setCustomer(T customer) {
        this.customer = customer;
    }
}
