package com.example.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {
    ArrayList<Customer> customers = new ArrayList<>();
    static int id = 0;
    public CustomerController(){
        customers.add(new Customer(id++,"Dara", "M", 20, "PP"));
        customers.add(new Customer(id++,"Nary", "F", 10, "KPS"));
        customers.add(new Customer(id++,"Sok", "M", 25, "SR"));
    }

    // get all customers
    @GetMapping("/customers")
    public ResponseEntity<?> getAllCustomer(){
        return ResponseEntity.ok(new CustomerResponse<ArrayList<Customer>>(
                LocalDateTime.now(),
                200,
                "Successfully.",
                customers
        ));
    }

    // insert into array list
    @PostMapping("/customers")
    public Customer insertCustomer(@RequestBody CustomerRequest customerRequest){
        Customer customer = new Customer();
        customer.setId(id++);
        customer.setCustomerName(customerRequest.getCustomerName());
        customer.setCustomerAge(customerRequest.getCustomerAge());
        customer.setCustomerGender(customerRequest.getCustomerGender());
        customer.setCustomerAddress(customer.getCustomerAddress());
        customers.add(customer);
        return null;
    }

    // get customers by id
    @GetMapping("/customers{id}")
    // public Customer getCustomerById(@PathVariable Integer id){ // we must write {id} as above path
    public Customer getCustomerById(@PathVariable("id") Integer cusId){
        for(Customer cus : customers){
            if(cus.getId() == cusId){
                return cus;
            }
        }
        return null;
    }

    // find customer by name
    @GetMapping("/customers/search")
    public Customer findCustomerById(@RequestParam String name){
        for(Customer cus : customers){
            if(cus.getCustomerName().equals(name)){
                return cus;
            }
        }
        return null;
    }

    //    @PutMapping   Update all in the row
    //    @DeleteMapping    Delete
    //    @PatchMapping  Update something of the row


    // update customer by id
    @PutMapping(value = "/customers/{customerId}")
    public Customer updateCustomerById(@PathVariable("customerId") Integer id, @RequestBody CustomerRequest customerRequest){
        for (Customer customer1 : customers){
            if (customer1.getId()==id){
                customer1.setCustomerName(customerRequest.getCustomerName());
                customer1.setCustomerAge(customerRequest.getCustomerAge());
                customer1.setCustomerGender(customerRequest.getCustomerGender());
                customer1.setCustomerAddress(customerRequest.getCustomerAddress());
                return customer1;
            }
        }
        return null;
    }

    // delete customer by id
    @DeleteMapping(value = "/customers/{customerId}")
    public String deleteById(@PathVariable("customerId") Integer cusId){
        for (Customer customer : customers){
            if( customer.getId() == cusId){
                customers.remove(customer);
                return "";
            }
        }
        return "";
    }

}
